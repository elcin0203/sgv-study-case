package com.sgv.studycase.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Slf4j
@Service
public class BaseRestTemplate {

    private final RestTemplate restTemplate = new RestTemplate();

    public <T> T processRequest(String url, Class<T> clazz) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<?> requestEntity = new HttpEntity<>(null, httpHeaders);
        ResponseEntity<T> responseEntity = this.restTemplate.exchange(url, HttpMethod.GET, requestEntity, clazz);
        return responseEntity.getBody();
    }
}
