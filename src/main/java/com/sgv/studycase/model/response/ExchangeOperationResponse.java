package com.sgv.studycase.model.response;

import com.sgv.studycase.enums.CoinEnum;
import com.sgv.studycase.enums.CurrencyEnum;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class ExchangeOperationResponse {

    private CurrencyEnum currency;
    private BigDecimal currencyAmount;
    private BigDecimal coinAmount;
    private CoinEnum coin;
    private Date date;
}
