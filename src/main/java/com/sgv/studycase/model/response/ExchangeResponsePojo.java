package com.sgv.studycase.model.response;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class ExchangeResponsePojo {
    private String symbol;
    private BigDecimal price_24h;
    private BigDecimal volume_24h;
    private BigDecimal last_trade_price;
}
