package com.sgv.studycase.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GenericResponse<T> implements Serializable {

    private static final long serialVersionUID = -2585207308495077914L;

    private transient T data;

    public static <T> GenericResponse<T> ok(T data) {
        return new GenericResponse<>(data);
    }

    public static <T> GenericResponse<T> empty() {
        return new GenericResponse<>();
    }
}
