package com.sgv.studycase.model.request;

import com.sgv.studycase.enums.CoinEnum;
import com.sgv.studycase.enums.CurrencyEnum;
import lombok.Getter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.math.BigDecimal;

@Getter
public class CoinExchangeRequest implements Serializable {

    private static final long serialVersionUID = -8217474503357067204L;

    @NotNull(message = "exchange.request.currencyRequired")
    private CurrencyEnum currency;

    @NotNull(message = "exchange.request.cryptoCurrencyRequired")
    private CoinEnum coin;

    @Min(value = 25, message = "exchange.request.amountMustBeGraterThan25")
    @Max(value = 5000, message = "exchange.request.amountMustBeSmallerThan5000")
    @Positive(message = "exchange.request.amountMustBePositive")
    @NotNull(message = "exchange.request.amountToBuyRequired")
    private BigDecimal amountToBuy;
}
