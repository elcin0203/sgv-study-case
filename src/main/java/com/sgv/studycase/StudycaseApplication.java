package com.sgv.studycase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.sgv")
public class StudycaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudycaseApplication.class, args);
	}

}
