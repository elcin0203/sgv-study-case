package com.sgv.studycase.controller;

import com.sgv.studycase.model.request.CoinExchangeRequest;
import com.sgv.studycase.model.response.ExchangeOperationResponse;
import com.sgv.studycase.model.response.GenericResponse;
import com.sgv.studycase.service.ExchangeService;
import com.sgv.studycase.swagger.GenerateSwaggerClient;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@GenerateSwaggerClient
@RestController
@RequiredArgsConstructor
public class CryptoCurrencyExchangeController {

    private final ExchangeService exchangeService;

    @PostMapping("/api/exchange")
    public GenericResponse<ExchangeOperationResponse> exchange(@Validated @RequestBody CoinExchangeRequest request) {
        return exchangeService.exchange(request);
    }
}
