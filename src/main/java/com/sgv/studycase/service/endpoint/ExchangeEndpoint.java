package com.sgv.studycase.service.endpoint;

public abstract class ExchangeEndpoint {

    public static final String COIN_CURRENCY_EXCHANGE_URL = "https://api.blockchain.com/v3/exchange/tickers/%s-%s";
}
