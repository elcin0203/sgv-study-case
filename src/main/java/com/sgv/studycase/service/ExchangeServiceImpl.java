package com.sgv.studycase.service;

import com.sgv.studycase.model.request.CoinExchangeRequest;
import com.sgv.studycase.model.response.ExchangeOperationResponse;
import com.sgv.studycase.model.response.ExchangeResponsePojo;
import com.sgv.studycase.model.response.GenericResponse;
import com.sgv.studycase.rest.BaseRestTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.util.Date;
import java.util.Objects;

import static com.sgv.studycase.service.endpoint.ExchangeEndpoint.COIN_CURRENCY_EXCHANGE_URL;

@Service
@RequiredArgsConstructor
public class ExchangeServiceImpl implements ExchangeService {

    private final BaseRestTemplate template;

    @Override
    public GenericResponse<ExchangeOperationResponse> exchange(CoinExchangeRequest request) {
        String url = String.format(COIN_CURRENCY_EXCHANGE_URL, request.getCoin(), request.getCurrency());
        ExchangeResponsePojo response = this.template.processRequest(url, ExchangeResponsePojo.class);
        if (Objects.isNull(response)) {
            return GenericResponse.empty();
        }
        ExchangeOperationResponse preparedResponse = getPreparedResponse(request, response);
        return GenericResponse.ok(preparedResponse);
    }

    private ExchangeOperationResponse getPreparedResponse(CoinExchangeRequest request, ExchangeResponsePojo response) {
        ExchangeOperationResponse operationResponse = new ExchangeOperationResponse();
        operationResponse.setCurrency(request.getCurrency());
        operationResponse.setCurrencyAmount(request.getAmountToBuy());
        operationResponse.setCoin(request.getCoin());
        operationResponse.setCoinAmount(request.getAmountToBuy().divide(response.getLast_trade_price(), 8, RoundingMode.FLOOR));
        operationResponse.setDate(new Date());
        return operationResponse;
    }
}
