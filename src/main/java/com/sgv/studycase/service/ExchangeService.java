package com.sgv.studycase.service;

import com.sgv.studycase.model.request.CoinExchangeRequest;
import com.sgv.studycase.model.response.ExchangeOperationResponse;
import com.sgv.studycase.model.response.GenericResponse;

public interface ExchangeService {

    GenericResponse<ExchangeOperationResponse> exchange(CoinExchangeRequest request);
}
