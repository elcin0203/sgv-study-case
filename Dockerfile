FROM openjdk:8-jdk-alpine
EXPOSE 9001
ADD target/sgv-study-case.jar sgv-study-case.jar
ENTRYPOINT ["java","-jar","/sgv-study-case.jar"]